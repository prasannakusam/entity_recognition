import argparse

import datetime

import os
import pandas as pd
import re
import spacy

from os import path

from os.path import basename

from bs4 import BeautifulSoup

from tika import parser


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='Cleanup html and recognize entities')
    parser.add_argument("source_path", help="Please specify path of the files "
                                            "you want to process", type=str)

    parser.add_argument("model_path", nargs='?', default='en',
                        help="Please specify the path of the spacy model "
                             "you want to load, default model is 'en'",
                        type=str)

    return parser.parse_args()


def recognize_persons(content, model):
    nlp = spacy.load(model)
    doc = nlp(content)

    persons = set()
    for ent in doc.ents:
        if ent.label_ in ['PERSON']:
            persons.add(ent.text)
    return persons


def recognize_entities(content, source_file):
    soup = BeautifulSoup(content, 'lxml')
    titles = soup.find_all(['h1', 'div', 'span'], {'id': re.compile('title')})

    if not titles:
        soup.find_all(['h1', 'div', 'span'], {'class': re.compile('title')})

    titles_set = set()
    for element in titles:
        titles_set.add(element.text)

    persons = recognize_persons(soup.text, model)

    entities = [(titles_set, persons, basename(source_file))]
    return entities


def is_file_empty(file):
    if os.path.isfile(file) and os.path.getsize(file) > 0:
        return True
    else:
        return False


if __name__ == '__main__':

    args = parse_arguments()
    source_path = args.source_path
    model = args.model_path

    output_path = path.join(source_path, 'target')
    entity_file = 'entities_' + datetime.datetime.now().strftime(
        "%Y%m%d-%H%M%S") + '.csv'
    entity_file = path.join(output_path, entity_file)

    for file in os.listdir(source_path):
        source_file = source_path + file
        if os.path.isfile(source_file) and not file.startswith('.'):
            with open(source_file, 'r', encoding='utf-8') as f:
                content = f.read()
                entities = recognize_entities(content, source_file)
                df = pd.DataFrame(entities, columns=['Titles', 'Persons', 'File_Name'])
                if not is_file_empty(entity_file):
                    df.to_csv(entity_file, index=False)
                else:
                    df.to_csv(entity_file, index=False, mode='a', header=False)