import argparse
import errno
import json
import logging
import os
import shutil
from os import path
from os.path import basename

import spacy
import sys
from bs4 import BeautifulSoup
from dateutil.parser import parse


def parse_arguments():
    parser = argparse.ArgumentParser(description='Cleanup html and recognize entities')
    parser.add_argument("source_html", help="Please specify path for html files", type=str)
    parser.add_argument("cleaned_text", help="Please specify path to clean text files", type=str)
    parser.add_argument("output_path", help="Please specify output path to write the extracted content", type=str)
    parser.add_argument("model_path", nargs='?', default='en',
                        help="Please specify the path of the spacy model you want to load, default model is 'en'",
                        type=str)

    return parser.parse_args()


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s - [%(name)s:%(lineno)d] - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.INFO)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger


logger = get_logger(__name__)


def recognize_persons(content, model):
    nlp = spacy.load(model)
    doc = nlp(content)

    persons = []
    for ent in doc.ents:
        if ent.label_ in ['PERSON']:
            persons.append(ent.text)
    return persons


def has_date(text):
    for e in text.splitlines():
        try:
            t = parse(e, fuzzy=True)
            return True
        except ValueError:
            return False


def recognize_entities(content, original_html, model):

    entities = {}

    titles_set = set()

    with open(original_html, 'r', encoding='utf-8') as f:
        original_content = f.read()
        soup = BeautifulSoup(original_content, 'lxml')

        # titles = soup.find_all(['h1', 'div', 'span'])
        #
        # for tag in titles:
        #     output = [x["title"] for x in tag]
        #     output
        #     # titles_set.add(tag.attrs['title'])

        # titles = soup.find_all(['h1', 'div', 'span'], {'id': re.compile('title')})
        #
        # titles.extend(soup.find_all(['h1', 'div', 'span'], {'class': re.compile('title')}))

        titles = soup.find_all(['title'])

        for element in titles:
            elementText = element.text
            sentences = elementText.split('\n')
            for sentence in sentences:
                if not has_date(sentence) and len(sentence.split()) > 5:
                    titles_set.add(sentence)

    persons = recognize_persons(content, model)

    entities['TI'] = list(titles_set)
    entities['AU'] = persons
    entities['AB'] = content
    entities['US'] = basename(original_html)
    return entities


def write_to_file(content, target_dir, file_name):
    try:
        os.makedirs(target_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    target_file = path.join(target_dir, file_name.replace('.txt','.json'))
    with open(target_file, 'w') as outfile:
        json.dump(content, outfile)


def create_dir(directory):
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def is_file_empty(file):
    if os.path.isfile(file) and os.path.getsize(file) > 0:
        return True
    else:
        return False


if __name__ == '__main__':

    args = parse_arguments()
    source_path = args.source_html
    cleaned_text_path = args.cleaned_text

    output_path = path.join(args.output_path, '')

    model = args.model_path

    # output_path = path.join(source_path, 'extracted-content')

    shutil.rmtree(output_path, ignore_errors=True)
    create_dir(output_path)

    # cleaned_text_path = path.join(source_path, 'output', 'text')

    for file in os.listdir(cleaned_text_path):
        cleaned_text_file = path.join(cleaned_text_path, file)

        original_html = path.join(source_path, file.replace('.txt', '.html'))

        if os.path.isfile(cleaned_text_file) and not file.startswith('.'):
            with open(cleaned_text_file, 'r', encoding='utf-8') as f:
                content = f.read()
                length_of_file = len(content)
                if len(content) >= 100000:
                    logger.info("Character length of file {} is more than spacy's limit".format(cleaned_text_file))
                    continue
                try:
                    entities = recognize_entities(content, original_html, model)
                    write_to_file(entities, output_path, file)
                except ValueError:
                    logger.error("Could not process file: {}".format(cleaned_text_file))